<?php

$puppetHOST= "localhost:14096";

require_once 'lib/function_vietin.php';


if(isset($_POST) && $_POST != null){
	$inputData = (object) $_POST;
} else {
	$getInput = trim(file_get_contents("php://input"));
	if($getInput != null){
		if(json_validator($getInput)){
			$inputData = json_decode($getInput);
		} else {
			exit("error 10000");
		}
	}
}

switch ($inputData->action) {
	case 'print_mutasi':
		// header('Content-Type: application/json');

		$fromUnix = $inputData->date1;
		$toUnix = $inputData->date2;

		$thisYear = date("Y",strtotime($inputData->date1));

		$inputData->date1 = date("d-m-Y",strtotime($inputData->date1));
		$inputData->date2 = date("d-m-Y",strtotime($inputData->date2));
		$inputData->current = date("d-m-Y");

		// $inputData->accnumber = str_replace("-", "", $inputData->accnumber);

		$targetString = "http://".$puppetHOST."/vietinbank/v1/puppet/load/".$inputData->loginname."/".$inputData->password."/".$inputData->date1."/".$inputData->date2;

		// print_r($targetString);
		// exit();

		$run = myCURL($targetString);
		// $run = getSampleData();
		if($run == "Init Required"){
			exit("error 10001 ".$run);
		} else if($run == "Failed To Login"){
			exit("error 10001 ".$run);
		}

		// print_r($run);
		// exit();

		$isJson = json_validator($run);
		if($isJson){
			$data = json_decode($run);
			$data->trans = array_reverse($data->trans);
			$startingBalance = floor(str_replace(".", "",$data->saldo));
			$debetMutasi = 0;
			$kreditMutasi = 0;
			foreach ($data->trans as $value) {
				$thisBalance = floor(str_replace(".", "", $value->nominal));
				if($value->type == "Debit"){
					$value->type = "D";
					$value->saldo = $value->balance;
					$startingBalance = $startingBalance - $thisBalance;
					$debetMutasi =  $debetMutasi + $thisBalance;
				} else {
					$value->type = "K";
					$value->saldo = $value->balance;
					$startingBalance = $startingBalance + $thisBalance;
					$kreditMutasi = $kreditMutasi + $thisBalance;
				}
				$reconfigmutation[] = $value;
			}

			$data->trans = array_reverse($reconfigmutation);
			header("Content-type: text/xml");
			echo '<?xml version="1.0" encoding="UTF-8"?>';
			echo "<response>";
			echo "<message>OPEN STATEMENT SUCCEED</message>";
			echo "<success>TRUE</success>";
			// echo "<err>0</err>";
			echo "<nama_rek>".$inputData->loginname."</nama_rek>";
			// echo "<nomor_rek>".$inputData->accnumber."</nomor_rek>";
			echo "<mata_uang>VND</mata_uang>";
			echo "<tgl_awal>".date("Ymd",strtotime($inputData->date1))."</tgl_awal>";
			echo "<tgl_akhir>".date("Ymd",strtotime($inputData->date2))."</tgl_akhir>";
			echo "<trans_count>".count($data->trans)."</trans_count>";
			echo "<saldo_awal>".floor(str_replace(".", "",$data->saldo))."</saldo_awal>";
			$count = 0;
			echo "<saldo_akhir>".floor($startingBalance)."</saldo_akhir>";
			echo "<mutasi_kredit>".floor($kreditMutasi)."</mutasi_kredit>";
			echo "<mutasi_debet>".floor($debetMutasi)."</mutasi_debet>";
			foreach ($data->trans as $value) {
				$count++;
				$kets = bank_fix_descr($value->description);
				// $keyString = $inputData->loginname."_".$value->transactionNumber."_".date("Ymd",strtotime(str_replace("/", "-", $value->date)))."_".str_replace(",","",$value->nominal)."_".str_replace(" ", "", $value->description);
				$keyString = $inputData->loginname."_".date("Ymd",strtotime(str_replace("/", "-", $value->date)))."_".str_replace(",","",$value->nominal)."_".str_replace(" ", "", $value->description);
				if(strpos($kets,"CT DEN:") !== false){
					if(strpos($kets,"VIETINBANK ") !== false){
						$kets = explode(" ",str_replace("CT DEN:","",$kets))[3];
					}else{
						$kets = explode(" ",str_replace("CT DEN:","",$kets))[1];
					}
				}else if(strpos($kets,"CT DI") !== false){
					$kets = explode(" ",str_replace("CT DI:","",$kets))[1];
				}else if(strpos($kets,"THOI GIAN GD:") !== false){ 
					if(strpos($kets,"TK ") !== false){
						$kets = explode(" ",$kets)[1];
					}else if(strpos($kets,"NAP ") !== false){
						if(strpos($kets,"NAP TIEN ") !== false){
							$kets = explode(" ",$kets)[2];
						}else{
							$kets = explode(" ",$kets)[1];
						}
					}else if(strpos($kets,"NAP TIEN VI CHINH DL ") !== false){
						$kets = explode(" ",str_replace("NAP TIEN VI CHINH DL ","",$kets))[0];
					}elseif(strpos($kets,"TEN NV ") !== false){
						$kets = explode(" ",str_replace("TEN NV ","",$kets))[0];
					}elseif(strpos($kets,"TNV ") !== false){
						$kets = explode(" ",str_replace("TNV ","",$kets))[0];
					}elseif(strpos($kets,"NCNV ") !== false){
						$kets = explode(" ",str_replace("NCNV ","",$kets))[0];
					}else{
						$kets = explode(" ",str_replace("THOI GIAN GD:","",$kets))[0];					
					}
				}else if(strpos($kets,"TAI BANKPLUS REQID ") !== false){
					$kets = explode(" ",$kets)[0];
				}else if(strpos($kets,"TK ") !== false){
					if(strpos($kets,"NAP TK TNV") !== false){
						$kets = explode(" ",$kets)[3];
					}else{
						$kets = explode(" ",$kets)[1];
					}
				}else if(strpos($kets,"NAP ") !== false){
					if(strpos($kets,"NAP TK TNV") !== false){
						$kets = explode(" ",$kets)[3];
					}else if(strpos($kets,"NAP NV") !== false){
						$kets = explode(" ",$kets)[2];
					}else if(strpos($kets,"NAP NICK") !== false){
						$kets = explode(" ",$kets)[2];
					}else if(strpos($kets,"NAP ACC ") !== false){
						$kets = explode(" ",$kets)[2];
					}else if(strpos($kets,"NAP TK ") !== false){
						$kets = explode(" ",$kets)[2];
					}else if(strpos($kets,"NAP CHIP ") !== false){
						if(strpos($kets,"NAP CHIP NHAN VAT ") !== false){
							$kets = explode(" ",$kets)[4];
						}else{
							$kets = explode(" ",$kets)[3];
						}
					}else if(strpos($kets,"NAP CHUYEN NHAN VAT ") !== false){
						$kets = explode(" ",$kets)[4];
					}else{
						$kets = explode(" ",$kets)[1];
					}
				}else if(strpos($kets,"IN GAME ") !== false){
					$kets = explode(" ",$kets)[2];
				}else if(strpos($kets,"GAME THAN POKER") !== false){
					$kets = explode(" ",$kets)[0];
				}elseif(strpos($kets,"TNV ") !== false){
					$kets = explode(" ",str_replace("TNV ","",$kets))[0];
				}elseif(strpos($kets,"MUA CHIP NV ") !== false){
					$kets = explode(" ",str_replace("MUA CHIP NV ","",$kets))[0];
				}else{
					$kets = mb_substr($kets, 0, 16);
				}

				echo "<trans>";
				echo "<no>".$count."</no>";
				// echo "<tgl>".date("Ymd",strtotime($value->date))."</tgl>";
				echo "<tgl>".$value->date."</tgl>";
				// echo "<ket>".$value->description."</ket>";
				$descption = explode(" ",$value->description);
				echo "<ket>".$value->description."</ket>";

				if(strpos($kets , "TNV_") !== false ) {
					echo "<kets>".str_replace("TNV_","",$kets)."</kets>";
				}else if(strpos($kets , "NCNV") !== false){
					echo "<kets>".str_replace("NCNV ","",$kets)."</kets>";
				}else {
					echo "<kets>".$kets."</kets>";
				}
				// echo "<kets>".str_replace("TNV_","",$kets)."</kets>";
				echo "<dk>".$value->type."</dk>";
				echo "<mutasi>".str_replace(".", "",$value->nominal)."</mutasi>";
				// echo "<saldo>".$value->availableBalance."</saldo>";
				echo "<saldo>".str_replace(".", "",$value->saldo)."</saldo>";
				echo "<cab></cab>";
				echo "<key>".$keyString."</key>";
				echo "</trans>";
			}

			echo "</response>";

		} else {
			// $run = str_replace("ax78.permatav2.mcxsecure.mime.services:14050", "ax78.permatav2.mcxsecure.mime.services", $run);
			echo $run;
		}

		break;

	case 'init':
		
		break;
	
	case 'debug':
		$fromUnix = $inputData->date1;
		$toUnix = $inputData->date2;

		$thisYear = date("Y",strtotime($inputData->date1));

		$inputData->date1 = date("d-m-Y",strtotime($inputData->date1));
		$inputData->date2 = date("d-m-Y",strtotime($inputData->date2));
		$inputData->current = date("d-m-Y");


		$inputData->accnumber = str_replace("-", "", $inputData->accnumber);

		$targetString = "http://ax78.permatav2.mcxsecure.mime.services/vietinbank/v1/puppet/load/".$inputData->loginname."/".$inputData->password."/".$inputData->accnumber."/".$inputData->date1."/".$inputData->date2."/".$inputData->current;

		$run = myCURL($targetString);
		if($response == "Init Required"){
			exit("error 10001 ".$response);
		} else if($response == "Failed To Login"){
			exit("error 10001 ".$response);
		}
		$isJson = json_validator($run);
		if($isJson){
			$data = json_decode($run);

			$startingBalance = floor(str_replace(",", "", strtok($data->saldo,".")));
			$debetMutasi = 0;
			$kreditMutasi = 0;
			foreach ($data->trans as $value) {
				$thisBalance = floor(str_replace(",", "", strtok($value->nominal,".")));
				if($value->type == "Debit"){
					$value->type = "D";
					$value->saldo = $startingBalance;
					$startingBalance = $startingBalance + $thisBalance;
					$debetMutasi =  $debetMutasi + $thisBalance;
				} else {
					$value->type = "K";
					$value->saldo = $startingBalance;
					$startingBalance = $startingBalance - $thisBalance;
					$kreditMutasi = $kreditMutasi + $thisBalance;
				}
				$reconfigmutation[] = $value;
			}

			$data->trans = array_reverse($reconfigmutation);
			header("Content-type: text/xml");
			echo '<?xml version="1.0" encoding="UTF-8"?>';
			echo "<response>";
			echo "<message>OPEN STATEMENT SUCCEED</message>";
			echo "<success>TRUE</success>";
			// echo "<err>0</err>";
			echo "<nama_rek>".$inputData->loginname."</nama_rek>";
			echo "<nomor_rek>".$inputData->accnumber."</nomor_rek>";
			echo "<mata_uang>VND</mata_uang>";
			echo "<tgl_awal>".date("Ymd",strtotime($inputData->date1))."</tgl_awal>";
			echo "<tgl_akhir>".date("Ymd",strtotime($inputData->date2))."</tgl_akhir>";
			echo "<trans_count>".count($data->trans)."</trans_count>";
			echo "<saldo_akhir>".floor($data->saldo)."</saldo_akhir>";
			echo "<saldo_awal>".floor($startingBalance)."</saldo_awal>";
			echo "<mutasi_kredit>".floor($kreditMutasi)."</mutasi_kredit>";
			echo "<mutasi_debet>".floor($startingBalance)."</mutasi_debet>";
			$count = 0;
			foreach ($data->trans as $value) {
				$count++;
				$kets = bank_fix_descr($value->description);
				$keyString =date("Ymd",strtotime($value->date))."_".str_replace(",","",$value->nominal)."_".str_replace(" ", "", $value->description);

				echo "<trans>";
				echo "<no>".$count."</no>";
				echo "<tgl>".date("Ymd",strtotime($value->date))."</tgl>";
				echo "<ket>".$value->description."</ket>";
				echo "<kets>".$kets."</kets>";
				echo "<dk>".$value->type."</dk>";
				echo "<mutasi>".str_replace(",", "",$value->nominal)."</mutasi>";
				// echo "<saldo>".$value->availableBalance."</saldo>";
				echo "<saldo>".$value->saldo."</saldo>";
				echo "<cab></cab>";
				echo "<key>".$keyString."</key>";
				echo "</trans>";
			}
			echo "</response>";

		} else {
			echo $run;
		}
		break;

	case 'quit':
		$targetString = "http://".$puppetHOST."/vietinbank/v1/puppet/logout/".$inputData->loginname;
		$run = myCURL($targetString);
		print_r($run);
		break;

	default:
		# code...
		break;
}

?>
